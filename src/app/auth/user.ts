export interface User {
  username: string;
  password: string;
  'https://hasura.io/jwt/claims'?: any;
}
